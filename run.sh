#!/bin/bash

set -e
export QT_QPA_PLATFORM=wayland
cd minimal
qmake minimal.pro
make
./minimal
